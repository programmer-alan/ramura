import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// component
import { LoginComponent } from './login/login.component';
import { AdminComponent } from '../admin/admin.component';
import { NotFoundComponent } from './not-found/not-found.component'
import { from } from 'rxjs';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
